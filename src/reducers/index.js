
import { API_SUCCESS, API_ERROR, SINGLE_DELETE, ALL_DELETE } from '../actions';
import { apiSuccess, apiError } from '../actions';

// reducer
export default function reducer(state = [], action){
   switch(action.type) {
       case API_SUCCESS: {
           return [
               ...state,
               action.payload
           ]
       }

       case API_ERROR: { 
           return state;
       }

       case SINGLE_DELETE: {
           return [
               ...state,
           ].filter((e,i) => {
            return i !== action.index;
           });
       }

       case ALL_DELETE: {
            return [];
       }

       default: {
           return state;
       }
   }
}

// function to post the entered URL to the impraise API and accept a shortcode with error handling
export function URLSubmitted(url) {
    return (dispatch) => {
        fetch('/shorten', {
            method:'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({url}),
        }).then(function(res){
            if(res.status === 201){
                return res.json();
            } else {
                // error
                throw new Error('API Error');
            }
        }).then(function(json) {
            // success
            dispatch(apiSuccess(json.shortcode, url));
            return json.shortcode;
        }).catch(function(err) {
            if(err.message  === 'API Error') {
                dispatch(apiError(url));            
            }
        }) 
    }
}