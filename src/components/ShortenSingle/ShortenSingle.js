import React, { Component } from  'react';
import { connect } from 'react-redux';
import './ShortenSingle.css';
import { singleDelete } from '../../actions';
import Close from '../../assets/img/close.svg';

const apiUrl = 'https://impraise-shorty.herokuapp.com';

class ShortenSingle extends Component {
    render() {
        return (
            <div className="shortened__single">
                <a href={`${apiUrl}/${this.props.shortcode}`} target="_blank" className="url-short">{`shooooort.com/${this.props.shortcode}`}</a>
                <p className="url-original">{this.props.originalUrl}</p>
                <button className="btn-delete__single" onClick={() => { this.props.handleDelete(this.props.index)}}><img className="icon__delete" src={Close} alt="Close"/></button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {};
};

const mapDispatchToAction = (dispatch) => {
    return {
        handleDelete(index) {
            dispatch(singleDelete(index))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToAction)(ShortenSingle);