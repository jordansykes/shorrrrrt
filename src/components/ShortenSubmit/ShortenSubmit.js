import React, { Component } from  'react';
import { connect } from 'react-redux'
import { URLSubmitted } from '../../reducers';
import './ShortenSubmit.css';


class ShortenSubmit extends Component {

    handleChange = (event) => {
        this.setState({value: event.target.value});
    }

    handleSubmit = (event) => {
        event.preventDefault();

        let inputUrl;

        if ((this.state) !== null && (this.state.value).length > 4) {
            
            if((this.state.value).includes('http://') || (this.state.value).includes('https://')){
                inputUrl = this.state.value
            } else {
                inputUrl =`http://${this.state.value}`;
            }
        } else {
            return;
        }

        this.props.URLSubmitted(inputUrl);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit} className="submit__wrapper">
                <input type="text" className="submit__input" onChange={this.handleChange} placeholder="Enter URL to squash here..."/>
                <button className="btn__submit"onClick={(event) => this.handleSubmit(event)}>Squash it</button> 
            </form>
        );
    }
}

const mapStateToProps = (state) => {
    return {};
};

const mapDispatchToAction = {
    URLSubmitted
};

export default connect(mapStateToProps, mapDispatchToAction)(ShortenSubmit);