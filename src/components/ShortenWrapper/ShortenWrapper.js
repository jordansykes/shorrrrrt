import React, { Component } from  'react';
import { connect } from 'react-redux';
import ShortenSingle from '../ShortenSingle/ShortenSingle';
import './ShortenWrapper.css';
import { allDelete } from "../../actions";

class ShortenWrapper extends Component {
    render() {
        // map over the urls return a shortened list item for each
        const urls = this.props.urls.map((url, index) => {
            return(
                <ShortenSingle {...url} key = {index} index = {index} />
            );
        });

        // define conditional variables
        let showDeleteAll;
        let nothingShortened;
        let shortenedTitle;

        // show a 'nothing squashed' messege is there are no urls to let the user know
        if (urls.length === 0) {
            nothingShortened = <div className="shortened__wrapper-placeholder">Looks like nothing has been squashed yet...</div>
        }
        // show a 'previously squashed' title when 1 or more urls are showing
        if(urls.length > 0) {
            shortenedTitle = <div className="shortened__wrapper-title">Previously Shortened URLS:</div>
        } 
        // as each shortened url has its own delete, only show clear history if there is more thna 1 url
        if (urls.length > 1) {
          showDeleteAll = <div className="btn-delete__all" onClick={() => { this.props.handleDeleteAll(this.props.urls)}}>Clear History</div>
        }

        return (
            <div>
                {shortenedTitle}
                <div className="shortened__wrapper">
                    {showDeleteAll}
                    {urls}
                    {nothingShortened}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    urls: state
});

const mapDispatchToAction = (dispatch) => ({
      handleDeleteAll() {
        dispatch(allDelete())
      }
  });

export default connect(mapStateToProps, mapDispatchToAction)(ShortenWrapper);