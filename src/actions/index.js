// actions
export const API_SUCCESS = 'API_SUCCESS';
export const API_ERROR = 'API_ERROR';
export const SINGLE_DELETE = 'SINGLE_DELETE';
export const ALL_DELETE = 'ALL_DELETE';

// action creators
export const apiSuccess = (shortcode, url) => ({
    type: API_SUCCESS,
    payload: {
        shortcode:shortcode,
        originalUrl: url
    }
});

export const apiError = (url) => ({
    type: API_ERROR,
    payload: {
        shortcode:'error',
        originalUrl: url
    }
});

export const singleDelete = (index) => ({
    type: SINGLE_DELETE,
    display: false,
    index: index
});

export const allDelete = () => ({
    type: ALL_DELETE,
    display: false
});