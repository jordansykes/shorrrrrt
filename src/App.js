import React, { Component } from 'react';
import ShortenSubmit from './components/ShortenSubmit/ShortenSubmit';
import ShortenWrapper from './components/ShortenWrapper/ShortenWrapper';
import Compress from './assets/img/compress.svg';
import './index.css';

class App extends Component {
  render() {
    return (
        <div className="app__wrapper">
            <div className="app__header">
                <img className="logo" src={Compress} alt="logo" />
                <p className="app__title">Short URL</p>
            </div>
            <ShortenSubmit />
            <div className="app__body">
                <ShortenWrapper />
            </div>
        </div>
    );
  }
}

export default App;
